Facter.add(:tomcat) do
  setcode do
    ctlhome = nil
    version = nil
    versiontomcat = nil
    $ctlhome = Facter::Util::Resolution.exec("ps -ef |grep tomcat |grep -v grep| awk -F'-Dcatalina.home=' '{sub(/ .*/,r'',$2);print $2}'")
    #$version = Facter::Util::Resolution.exec("ls -l #$ctlhome/bin/version.sh")
    $version = Facter::Util::Resolution.exec("#$ctlhome/bin/version.sh")
    $versiontomcat = Facter::Util::Resolution.exec("#$version |grep Server") 
    $versiontomcat

  end
end
